# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

config :book_bot_web,
  generators: [context_app: :book_bot]

# Configures the endpoint
config :book_bot_web, BookBotWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "qAUmB1+nnV9cIRGy2cA33hgJZ/ytfOOqVk18RuHA2v96ZBFpH8+z/uwR2qOrmhhq",
  render_errors: [view: BookBotWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: BookBot.PubSub,
  live_view: [signing_salt: "VAoRRSDn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"


defmodule EnvVars do
  def set(var) do
    System.get_env(var) ||
      raise """
      environment variable #{var} is missing.
      """
  end
end

config :messenger, page_access_token: EnvVars.set("FACEBOOK_PAGE_ACCESS_TOKEN")
config :messenger, base_url: EnvVars.set("FACEBOOK_GRAPH_API_HOSTNAME")
config :messenger, :webhook,
  verify_token: EnvVars.set("MESSENGER_WEBHOOK_VERIFY_TOKEN")

config :goodreads, api_key: EnvVars.set("GOODREADS_API_KEY")
config :goodreads, api_secret: EnvVars.set("GOODREADS_API_SECRET")

