defmodule MessengerTest do
  use ExUnit.Case
  doctest Messenger

  test "base_url/0" do
    assert Messenger.base_url() == Application.get_env(:messenger, :base_url)
  end
end
