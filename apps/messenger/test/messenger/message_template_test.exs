defmodule Messenger.MessageTemplateTest do
  use ExUnit.Case, async: true

  alias Messenger.MessageTemplate

  test "greet/1 greets user by name" do
    assert MessageTemplate.greet("Adrian") ==
      "Hi, Adrian! Welcome to Book Reviews where you can search for books " <>
        "and get their reviews."
  end

  test "search_question/0 asks user to choose to search by title or by Goodreads ID" do
    assert MessageTemplate.search_question() == "How do you want to find the books?"
  end

  test "search_by_title_instruction/0 instructs user how to search by title" do
    assert MessageTemplate.search_by_title_instruction() ==
      "Type \"title: <your search term>\" to search for books that match the term"
  end

  test "find_by_id_instruction/0 instructs user how to get reviews by book id" do
    assert MessageTemplate.find_by_id_instruction() ==
      "Type \"id: <goodreads book id>\" to get reviews of the book"
  end
end
