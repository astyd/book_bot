defmodule Messenger.RequestInfoTest do
  use ExUnit.Case

  alias Messenger.RequestInfo

  test "parse/1 parses request body into struct" do
    request_body = %{
      "entry" => [%{
        "id" => "102791291505062",
        "messaging" => [%{
          "mid" => "m_wCQmmFEAlWgXPLlxXgPDF0DvbK5-kP22IqfTkYQyRSiUzo_V9jmvVkDyPEkJx4hBj1uXHDJDa44aedIb1df4jw",
          "message" => %{"text" => "test"},
          "recipient" => %{"id" => "102791291505062"},
          "sender" => %{"id" => "3157870977606262"},
          "timestamp" => 1593842305040
        }],
        "time" => 1593842305459

      }],
      "object" => "page"
    }

    assert RequestInfo.parse(request_body) ==
      [%RequestInfo{sender_psid: "3157870977606262", text: "test", payload: nil}]
  end
end
