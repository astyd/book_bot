defmodule Messenger.ReplyBuilderTest do
  use ExUnit.Case

  alias Messenger.ReplyBuilder
  alias Goodreads.Book

  test "simple_text/1 builds simple text reply" do
    assert ReplyBuilder.simple_text("Hello!") == %{text: "Hello!"}
  end

  test "quick_replies/2 builds quick replies" do
    replies = ReplyBuilder.quick_replies("Choose color!", ["Red", "Green", "Blue"])
    assert replies == %{
      text: "Choose color!",
      quick_replies: [
        %{
          content_type: "text",
          title: "Red",
          payload: "red",
        },
        %{
          content_type: "text",
          title: "Green",
          payload: "green",
        },
        %{
          content_type: "text",
          title: "Blue",
          payload: "blue",
        },
      ]
    }
  end

  test "card_elements/2 builds generic template" do
    objects = [
      %Goodreads.Book{ 
        author: "David Eddings",
        id: "44687",
        image: "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1217735909l/44687._SX98_.jpg",
        title: "Enchanters' End Game (The Belgariad, #5)"
      }
    ]
    replies = ReplyBuilder.card_elements(objects)
    assert replies == %{
      attachment: %{
        type: "template",
        payload: %{
          template_type: "generic",
          elements: [%{
            title: "Enchanters' End Game (The Belgariad, #5)",
            image_url: "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1217735909l/44687._SX98_.jpg",
            subtitle: "David Eddings",
            buttons: [
              %{
                type: "postback",
                title: "Get Reviews",
                payload: "get_reviews_44687"
              )
            ]
          }],
        }
      },
    }
  end
end
