defmodule Messenger do
  alias Messenger.{
    MessageTemplate,
    ReplyBuilder,
    RequestInfo,
  }

  @base_url Application.get_env(:messenger, :base_url)
  @page_access_token Application.get_env(:messenger, :page_access_token)
  @send_api_endpoint (@base_url <> "/v7.0/me/messages")

  def base_url, do: @base_url
  def page_access_token, do: @page_access_token
  def send_api_endpoint, do: @send_api_endpoint

  def get_started(request_info) do
    name = sender_name(request_info.sender_psid)

    greet =
      MessageTemplate.greet(name)
      |> ReplyBuilder.simple_text()

    call_send_api(request_info.sender_psid, greet)

    question =
      MessageTemplate.search_question()
      |> ReplyBuilder.quick_replies(["By Title", "By Goodreads ID"])

    call_send_api(request_info.sender_psid, question)
  end

  def by_title(request_info) do
    instruction =
      MessageTemplate.search_by_title_instruction()
      |> ReplyBuilder.simple_text()

    call_send_api(request_info.sender_psid, instruction)
  end

  def sender_name(sender_psid) do
    endpoint = base_url() <> "/" <> sender_psid
    query_params = %{
      fields: "first_name",
      access_token: Application.get_env(:messenger, :page_access_token)
    }

    HTTPoison.start
    with {:ok, %{body: raw_body}} <- HTTPoison.get(endpoint, [], params: query_params),
         {:ok, %{first_name: first_name}} <- Poison.decode(raw_body, keys: :atoms) do
      first_name
    else
      {:error, _reason} ->
        "User"
    end
  end

  def handle_message(%RequestInfo{payload: payload} = request_info) when is_nil(payload) do
    case request_info.text do
      "title:" <> title ->
        title
        |> String.trim()
        |> Goodreads.search_by_title()
        |> Enum.each(fn book ->
          reply = ReplyBuilder.card_elements([book])
          call_send_api(request_info.sender_psid, reply)
        end)

      "id:" <> _id ->
        nil

      text ->
        reply = ReplyBuilder.simple_text("You sent #{inspect(text)}.")
        call_send_api(request_info.sender_psid, reply)
    end
  end

  def handle_message(%RequestInfo{} = request_info) do
    case request_info.payload do
      "get_started" -> get_started(request_info)
      "by_title" -> by_title(request_info)
      _ -> :unknown
    end
  end

  def call_send_api(sender_psid, reply) do
    endpoint = send_api_endpoint()
    query_params = %{
      access_token: page_access_token()
    }
    headers = [
      {"Content-Type", "application/json"}
    ]
    body = %{
      recipient: %{
        id: sender_psid
      },
      message: reply
    } |> Poison.encode!

    IO.puts("Endpoint:\n #{inspect(endpoint)}")
    IO.puts("Body:\n #{inspect(body)}")
    IO.puts("Headers:\n #{inspect(headers)}")
    IO.puts("Params:\n #{inspect(query_params)}")

    if Application.get_env(:messenger, :webhook)[:verify_token] != "<YOUR_VERIFY_TOKEN>" do
      HTTPoison.start
      case HTTPoison.post(endpoint, body, headers, params: query_params) do
        {:ok, _body} ->
          IO.puts("OK")
        {:error, _reason} ->
          IO.puts("NOT OK")
      end
    end
  end
end
