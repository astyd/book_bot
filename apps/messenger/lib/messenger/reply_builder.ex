defmodule Messenger.ReplyBuilder do
  def simple_text(message) do
    %{text: message}
  end

  def quick_replies(message, options) do
    replies =
      options
      |> Enum.map(fn option ->
        payload = underscore(option)

        %{
          content_type: "text",
          title: option,
          payload: payload,
        }
      end)

    %{
      text: message,
      quick_replies: replies,
    }
  end

  def card_elements(objects) do
    elements =
      objects
      |> Enum.map(fn object ->
        %{
          title: object.title,
          image_url: object.image,
          subtitle: object.author,
          buttons: [
            postback_button(
              "Get Reviews",
              "get_reviews_#{object.id}"
            )
          ]
        }
      end)

    %{
      attachment: %{
        type: "template",
        payload: %{
          template_type: "generic",
          elements: elements,
        }
      },
    }
  end

  defp postback_button(title, payload) do
    %{
      type: "postback",
      title: title,
      payload: payload,
    }
  end

  defp underscore(string) do
    string
    |> Macro.underscore()
    |> String.replace("\s", "")
  end
end
