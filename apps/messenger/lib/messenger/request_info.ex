defmodule Messenger.RequestInfo do
  defstruct [:sender_psid, :text, :payload]

  def parse(%{"object" => "page"} = request_body) do
    request_body["entry"]
    |> Enum.map(fn entry ->
      messaging =
        entry
        |> Map.get("messaging")
        |> List.first

      sender_psid =
        messaging
        |> get_in(["sender","id"])

      text =
        messaging
        |> get_in(["message","text"])

      text = if text do
        String.trim(text)
      end

      payload =
        (messaging
        |> get_in(["postback","payload"])) ||
          (messaging
          |> get_in(["message", "quick_reply", "payload"]))

      %__MODULE__{
        sender_psid: sender_psid,
        text: text,
        payload: payload,
      }
    end)
  end
end
