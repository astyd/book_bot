defmodule Messenger.MessageTemplate do
  import Messenger.Gettext, only: [gettext: 2]

  def greet(name) do
    gettext(
      "Hi, %{name}! Welcome to Book Reviews where you can search " <>
        "for books and get their reviews.", name: name
    )
  end

  def search_question do
    gettext("How do you want to find the books?", [])
  end

  def search_by_title_instruction do
    gettext(
      "Type \"title: <your search term>\" to search for books that match " <>
        "the term", []
    )
  end

  def find_by_id_instruction do
    gettext("Type \"id: <goodreads book id>\" to get reviews of the book", [])
  end
end
