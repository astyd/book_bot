defmodule BookBotWeb.WebhookController do
  use BookBotWeb, :controller

  alias Messenger.{
    RequestInfo,
  }

  def index(conn, params) do
    IO.puts("")
    IO.puts("")
    IO.inspect(params)
    IO.puts("")
    IO.puts("")

    verify_token = Application.get_env(:messenger, :webhook)[:verify_token]
    if params["hub.verify_token"] == verify_token do
      send_resp(conn, 200, params["hub.challenge"])
    else
      send_resp(conn, 403, "")
    end
  end

  def create(conn, params) do
    IO.puts("")
    IO.puts("")
    IO.inspect(params)
    IO.puts("")
    IO.puts("")

    params
    |> RequestInfo.parse
    |> Enum.each(fn request_info ->
      Messenger.handle_message(request_info)
    end)

    send_resp(conn, 200, "EVENT_RECEIVED")
  end
end
