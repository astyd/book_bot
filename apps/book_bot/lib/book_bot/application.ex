defmodule BookBot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: BookBot.PubSub}
      # Start a worker by calling: BookBot.Worker.start_link(arg)
      # {BookBot.Worker, arg}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: BookBot.Supervisor)
  end
end
