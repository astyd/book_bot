defmodule Goodreads.Book do
  defstruct [:id, :image, :title, :author]
end
