defmodule Goodreads do
  import SweetXml

  alias Goodreads.Book

  @search_endpoint "https://www.goodreads.com/search/index.xml"
  @show_base_url "https://www.goodreads.com/book/show/"
  @show_endpoint "https://www.goodreads.com/search/index.xml"
  @api_key Application.get_env(:goodreads, :api_key)

  def search_endpoint, do: @search_endpoint
  def api_key, do: @api_key
  def show_endpoint(id) do
    @show_base_url <> id <> ".xml"
  end

  def search_by_title(title, limit \\ 5) do
    doc =
      title
      |> search_by_title_path()
      |> fetch_xml()

    doc
    |> stream_tags(:work)
    |> Stream.map(fn {_, doc} ->
      book =
        doc
        |> xpath(
          ~x"./best_book",
          id: ~x"./id/text()",
          title: ~x"./title/text()",
          author: ~x"./author/name/text()",
          image: ~x"./image_url/text()"
        )
        |> Enum.map(fn {key, value} ->
          {key, List.to_string(value)}
        end)
      struct(Book, book)
    end)
    |> Enum.take(limit)
  end

  defp search_by_title_path(title) do
    "#{search_endpoint()}?" <>
      URI.encode_query([{"search[field]", "title"}, key: api_key(), q: title])
  end

  defp fetch_xml(url) do
    {:ok, {_, _, body}} = :httpc.request(String.to_charlist(url))

    body
  end
end
